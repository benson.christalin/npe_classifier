select napl.normalized_party_id, 
SUM(CASE WHEN napl.npe_sub_categories = 'Individual' THEN 1 ELSE 0 END) AS "Individual",
SUM(CASE WHEN napl.npe_sub_categories = 'PAE - Institutional' THEN 1 ELSE 0 END) AS "PAE - Institutional",
SUM(CASE WHEN napl.npe_sub_categories = 'Undetermined' THEN 1 ELSE 0 END) AS "Undetermined",
SUM(CASE WHEN napl.npe_sub_categories = 'PAE - Ad Hoc' THEN 1 ELSE 0 END) AS "PAE - Ad Hoc",
SUM(CASE WHEN napl.npe_sub_categories = 'OC' THEN 1 ELSE 0 END) AS "Operating Company",
SUM(CASE WHEN napl.npe_sub_categories = 'Industry Consortium' THEN 1 ELSE 0 END) AS "Industry Consortium",
SUM(CASE WHEN napl.npe_sub_categories = 'University' THEN 1 ELSE 0 END) AS "University",
SUM(CASE WHEN napl.npe_sub_categories = 'unknown' THEN 1 ELSE 0 END) AS "Unknown"
from npe_processed.table_npe_all_party_labeling napl
group by 1;
