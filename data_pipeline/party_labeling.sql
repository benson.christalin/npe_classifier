create table npe_processed.table_npe_all_party_labeling as
select pp.normalized_party_id, 
(CASE
		WHEN lpisam.npe_sub_categories IS NULL THEN
		'unknown'
		ELSE
		lpisam.npe_sub_categories
		END) AS npe_sub_categories
from lex_processing.processed_parties pp 
left join npe_processed.table_lex_party_id_stanford_asserter_map lpisam on pp.normalized_party_id = lpisam.normalized_party_id;
